import asyncio
import logging
import os
from datetime import datetime
from random import randint

import aiohttp

CONTROLLER_API_URL = os.environ.get("CONTROLLER_API_URL", "http://controller:8000/api/")
FORMAT = "%(asctime)-15s %(message)s"
logging.basicConfig(format=FORMAT)
logger = logging.getLogger(__name__)


async def main():
    while True:
        data = {
            "datetime": datetime.now().strftime("%Y%m%dT%H%M.%f"),
            "payload": randint(1, 100),
        }
        try:
            async with aiohttp.ClientSession() as session:
                async with session.post(
                    CONTROLLER_API_URL + "sensor/", json=data
                ) as response:
                    logger.info(await response.text())
        except aiohttp.ClientConnectionError:
            logger.error("Server not respond")
            await asyncio.sleep(1)
        except KeyboardInterrupt:
            break
        else:
            await asyncio.sleep(0.5)


asyncio.run(main())
