import asyncio
import logging

import os

FORMAT = "%(asctime)-15s %(message)s"
logging.basicConfig(format=FORMAT, level=logging.INFO)
logger = logging.getLogger(__name__)

CONTROLLER_HOST = os.environ.get("CONTROLLER_HOST", "controller")
CONTROLLER_PORT = int(os.environ.get("CONTROLLER_PORT", 8888))


class ManipulatorProtocol(asyncio.Protocol):
    def __init__(self, message, on_lost, loop):
        self.message = message
        self.loop = loop
        self.on_lost = on_lost

    def connection_made(self, transport):
        transport.write(self.message.encode())
        logger.info("Data sent: {!r}".format(self.message))

    def data_received(self, data):
        logger.info("Data received: {!r}".format(data.decode()))

    def connection_lost(self, exc):
        logger.info("The server closed the connection")
        self.on_lost.set_result(True)


async def main():
    loop = asyncio.get_running_loop()

    on_lost = loop.create_future()
    message = "manipulator"
    while True:
        transport, protocol = await loop.create_connection(
            lambda: ManipulatorProtocol(message, on_lost, loop),
            CONTROLLER_HOST,
            CONTROLLER_PORT,
        )
        await asyncio.sleep(5)
    try:
        await on_lost
    finally:
        transport.close()


asyncio.run(main())
