### Usage:
`docker-compose up --build`

Open [http://localhost:8000](http://localhost:8000) in browser.

Before first run rename `.dockerenv.example` to `.dockerenv` in docker folders.