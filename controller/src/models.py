from tortoise.models import Model
from tortoise import fields


class SensorValue(Model):
    id = fields.IntField(pk=True)
    datetime = fields.DatetimeField()
    payload = fields.IntField()

    def __str__(self):
        return str(self.id)


class State(Model):
    id = fields.IntField(pk=True)
    sensor_value = fields.ForeignKeyField("models.SensorValue", related_name="states")
    datetime = fields.DatetimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id)
