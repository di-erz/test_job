import asyncio
import json
import logging
import os
from datetime import datetime, timedelta
from random import randint
from time import sleep

import aiohttp
import aiohttp_jinja2
import jinja2
from aiojobs.aiohttp import setup, spawn
from tortoise import Tortoise

from models import SensorValue, State

FORMAT = "%(asctime)-15s %(message)s"
logging.basicConfig(format=FORMAT, level=logging.INFO)
logger = logging.getLogger(__name__)

routes = aiohttp.web.RouteTableDef()


@routes.get("/ws/")
async def websocket_handler(request):
    logger.info("Websocket connection starting")
    ws = aiohttp.web.WebSocketResponse()
    await ws.prepare(request)
    logger.info("Websocket connection ready")

    while True:
        await ws.send_json({"payload": "none"})
        async for msg in ws:
            logger.info(f"Message received: {msg}")
            if msg.type == aiohttp.WSMsgType.TEXT:
                if msg.data == "close":
                    await ws.close()
                else:
                    sensor_value = await SensorValue.all().order_by("-datetime").first()
                    await State.create(sensor_value=sensor_value)
                    if sensor_value.payload < 40:
                        payload = "up"
                    elif sensor_value.payload > 60:
                        payload = "down"
                    else:
                        payload = "none"
                    await ws.send_json({"payload": payload})
            await asyncio.sleep(5)

    logger.info("Websocket connection closed")
    return ws


@routes.post("/api/sensor/")
async def handle_sensor(request):
    latest = await request.json()
    logger.info(f"Update sensors data {latest}")
    await SensorValue.create(
        datetime=datetime.strptime(latest["datetime"], "%Y%m%dT%H%M.%f"),
        payload=latest["payload"],
    )
    return aiohttp.web.json_response({"status": "ok"})


@routes.get("/")
@aiohttp_jinja2.template("index.html")
async def handle_index(request):
    state = await State.all().order_by("-datetime").first()
    if state:
        await state.fetch_related("sensor_value")
        if state.sensor_value.payload < 40:
            sensor_value_payload_type = "UP"
        elif state.sensor_value.payload > 60:
            sensor_value_payload_type = "DOWN"
        else:
            sensor_value_payload_type = "NONE"

        return {
            "id": state.id,
            "datetime": state.datetime,
            "sensor_value_id": state.sensor_value.id,
            "sensor_value_datetime": state.sensor_value.datetime,
            "sensor_value_payload": state.sensor_value.payload,
            "sensor_value_payload_type": sensor_value_payload_type,
        }
    else:
        return {}


async def handle_manipulator(reader, writer):
    data = await reader.read(100)
    message = data.decode()
    addr = writer.get_extra_info("peername")

    logger.info(f"Received {message} from {addr}")
    if data == b"manipulator":
        sensor_value = await SensorValue.all().order_by("-datetime").first()
        state = await State.create(sensor_value=sensor_value)
        if state:
            if sensor_value.payload < 40:
                sensor_value_payload_type = "up"
            elif sensor_value.payload > 60:
                sensor_value_payload_type = "down"
            else:
                sensor_value_payload_type = "none"
        else:
            sensor_value_payload_type = "none"
        message = json.dumps(
            {
                "datetime": datetime.now().strftime("%Y%m%dT%H%M.%f"),
                "payload": sensor_value_payload_type,
            }
        )

    logger.info(f"Send: {message}")
    writer.write(bytes(message, "utf-8"))
    await writer.drain()


async def main():
    logger.error("Init DB")

    await Tortoise.init(db_url="sqlite://db.sqlite3", modules={"models": ["models"]})
    await Tortoise.generate_schemas()

    logger.error("Run controller server")

    app = aiohttp.web.Application()
    app.add_routes(routes)
    aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader("./templates"))
    runner = aiohttp.web.AppRunner(app)

    logger.error("Run api server setup")
    await runner.setup()

    site = aiohttp.web.TCPSite(runner, host="0.0.0.0", port=8000)
    logger.error("Run tcp site")
    await site.start()

    server = await asyncio.start_server(handle_manipulator, "0.0.0.0", 8888)
    async with server:
        await server.serve_forever()


asyncio.run(main())
